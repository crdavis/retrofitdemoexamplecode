package com.example.retrofitdemoexamplecode.data.repository

import com.example.retrofitdemoexamplecode.data.CreditCardResponse
import com.example.retrofitdemoexamplecode.data.RetrofitInstance

class CreditCardRepository {
    private val creditCardService = RetrofitInstance.creditCardService

    suspend fun getCreditCards(): CreditCardResponse {
        return creditCardService.getCreditCards()
    }
}